<?php

/**
 * @file
 * Provides the Fontawesome options to get them saved then rendered in blocks.
 */

/**
 * Implements hook_help().
 */
function fontawesome_block_icons_help($path, $arg) {
  switch ($path) {
    case 'admin/help#fontawesome_block_icons':
      return '<p>' . t('This module will help you get your blocks look better by adding Fontawesome icons to each one .. Configure your block to see the options there.') . '</p>';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function fontawesome_block_icons_form_block_admin_configure_alter(&$form, &$form_state, &$form_id) {
  $uniq_name = $form['module']['#value'] . '_' . $form['delta']['#value'];
  $form['visibility']['fontawesome_block_icons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fontawesome Block Icon'),
    '#weight' => 200,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['visibility']['fontawesome_block_icons']['fontawesome_block_icons_options'] = array(
    '#type' => 'textfield',
    '#title' => t('Fontawesome Icon:'),
    '#default_value' => variable_get('fontawesome_block_icons_options_' . $uniq_name, ''),
    '#description' => t('You only need <b>fa-xxxx</b> out of the fontawesome class(fa-drupal):<a href="@examples-link">Examples</a>', array(
      '@examples-link' => 'https://fortawesome.github.io/Font-Awesome/icons/',
    )),
  );
  $form['visibility']['fontawesome_block_icons']['fontawesome_block_icons_options_size'] = array(
    '#type' => 'select',
    '#title' => t('Size Of Icon:'),
    '#options' => array(
      'fa-lg' => 'fa-lg',
      'fa-2x' => 'fa-2x',
      'fa-3x' => 'fa-3x',
      'fa-4x' => 'fa-4x',
      'fa-5x' => 'fa-5x',
    ),
    '#default_value' => variable_get('fontawesome_block_icons_options_size_' . $uniq_name, 'fa-2x'),
    '#description' => t('Set the size of fontawesome icon .. you can find more about this:<a href="@examples-link">Examples</a>.', array(
      '@examples-link' => 'https://fortawesome.github.io/Font-Awesome/examples/',
    )),
  );
  $form['visibility']['fontawesome_block_icons']['fontawesome_block_icons_options_custom_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom CSS class:'),
    '#default_value' => variable_get('fontawesome_block_icons_options_custom_css_' . $uniq_name, ''),
    '#size' => 30,
    '#maxlength' => 128,
    '#description' => t('In case you need to customize your icon'),
  );
  $form['#submit'][] = 'fontawesome_block_icons_update';
}

/**
 * Form submission handler for hook_form_FORM_ID_alter().
 *
 * @see hook_form_FORM_ID_alter()
 */
function fontawesome_block_icons_update($form_id, &$form_state) {
  $uniq_name = $form_state['values']['module'] . '_' . $form_state['values']['delta'];
  $icons_value = $form_state['values']['fontawesome_block_icons_options'];
  variable_set('fontawesome_block_icons_options_' . $uniq_name, $icons_value);
  $icons_value = $form_state['values']['fontawesome_block_icons_options_size'];
  variable_set('fontawesome_block_icons_options_size_' . $uniq_name, $icons_value);
  $icons_value = $form_state['values']['fontawesome_block_icons_options_custom_css'];
  variable_set('fontawesome_block_icons_options_custom_css_' . $uniq_name, $icons_value);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function fontawesome_block_icons_preprocess_block(&$variables) {
  $uniq_name = $variables['elements']['#block']->module . '_' . $variables['elements']['#block']->delta;
  if (variable_get('fontawesome_block_icons_options_' . $uniq_name)) {
    $elements = $variables['elements']['#block'];
    $attributes = array(
      'class' => array('fa'),
    );
    $vars = array(
      'fontawesome_block_icons_options_',
      'fontawesome_block_icons_options_size_',
      'fontawesome_block_icons_options_custom_css_',
    );
    foreach ($vars as $variable) {
      $value = variable_get($variable . $uniq_name, '');
      if (!empty($value)) {
        $attributes['class'][] = $value;
      }
    }
    $elements->subject = '<i' . drupal_attributes($attributes) . '></i><span>' . $elements->subject . '</span>';
  }
}
