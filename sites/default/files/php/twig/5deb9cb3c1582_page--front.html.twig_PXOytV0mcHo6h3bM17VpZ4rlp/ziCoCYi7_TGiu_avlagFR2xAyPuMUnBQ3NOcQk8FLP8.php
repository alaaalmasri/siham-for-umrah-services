<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/shiam/page--front.html.twig */
class __TwigTemplate_cd9630d7f25ac42c9a5e71af1675a59c9049c257363998b57435cf4e53ab70cf extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 2];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"background\">
    ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slider", [])), "html", null, true);
        echo "

    <div class=\"header-container\">
        <div class=\"row\">
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"logo-img\">
                    <img src=\"themes/shiam/assets/images/logo%20(1).png\" class=\"img-responsive\">
                </div>
            </div>
            <div class=\"col-md-9 col-sm-12 col-xs-12\">
              
                <div class=\"main-nav\">
                    <nav class=\"navbar navbar-expand-lg navbar-light \">
                        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                            <span class=\"navbar\"></span>
                        </button>

                        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">

                          ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navbar", [])), "html", null, true);
        echo "
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class=\"slider-content\">
        <h2><b><i>UMRAH & Tourism</i></b></h2>
        <h3>in Saudi Arabia</h3>
        <div class=\"slider-read-more-btn\">
            <a href=\"#\">Read More</a>
        </div>
    </div>
</div>
<div class=\"sheam-introduction\">
    <div class=\"container\">
     ";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "about_us", [])), "html", null, true);
        echo "
    </div>
    <div class=\"background2\"></div>
    <div class=\"background3\">
        <div class=\"UMRAH-section\">
            <div class=\"container\">
           ";
        // line 44
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "umrahpackages", [])), "html", null, true);
        echo "
            </div>
           
            <div class=\"background-img\">
              
                    <div class=\"omra-packages\">
   <div class=\"carousel carousel-slider\">
                        ";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "umra_slickslider", [])), "html", null, true);
        echo "
                               </div>
                        </div>
                          <div class=\"prev\"><i class=\"fas fa-angle-left\"></i></div>
                          <div class=\"next\"><i class=\"fas fa-angle-right\"></i></div>

                           
            
                    </div>
                    
            
            </div>
        </div>
    </div>
<div class=\"torurism\">

    <div class=\"sheam-introduction-content\">
        <div class=\"container\">
       ";
        // line 69
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "tourism", [])), "html", null, true);
        echo "
        </div>
        
    
        <div class=\"packages\">
           <div class=\"carousel carousel-slider\">

        ";
        // line 76
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "tourism_slider", [])), "html", null, true);
        echo "
            </div>
            <div class=\"prev\"><i class=\"fas fa-angle-left\"></i></div>
                          <div class=\"next\"><i class=\"fas fa-angle-right\"></i></div>

        </div>
    
    </div>
</div>
<footer>
    <div class=\"footer\">
        <div class=\"row\">
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"shema-address\">

                </div>
                <h3>How to Find US</h3>
                <div class=\"sheam-adress-content\">
                    <img src=\"themes/shiam/assets/images/logo1.png\" class=\"img-responsive\">
                    <h2>ADDRESS</h2>
                    <div class=\"adress\">
                        ";
        // line 97
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "address", [])), "html", null, true);
        echo "
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"phone-number\">
                    <img src=\"themes/shiam/assets/images/icon2.png\" class=\"img-responsive\">
                    <h3>Call US</h3>
                    <div class=\"phone-numbers\">
                      ";
        // line 106
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "phone", [])), "html", null, true);
        echo "
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"social-title\">
                    <h2><b>FOLLOW US</b></h2>
                </div>

                <div id=\"social-media\">
                 ";
        // line 116
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "social_media", [])), "html", null, true);
        echo "
                </div>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"news-letter-title\">
                    <h2><b>Newsletter</b></h2>
                </div>
                <div class=\"news-letter-form\">
                    <div class=\"form\">
                        <input type=\"text\" class=\"form-control\" id=\"search\" placeholder=\"name\">
                        <div class=\"submit\">
                            <a href=\"#\">submit</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <div class=\"copyrights\">
        <p>Copyright @Fikra. All Right Reserved.</p>
    </div>
</footer>

";
    }

    public function getTemplateName()
    {
        return "themes/shiam/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 116,  186 => 106,  174 => 97,  150 => 76,  140 => 69,  119 => 51,  109 => 44,  100 => 38,  80 => 21,  58 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/shiam/page--front.html.twig", "C:\\wamp64\\www\\siham\\themes\\shiam\\page--front.html.twig");
    }
}
