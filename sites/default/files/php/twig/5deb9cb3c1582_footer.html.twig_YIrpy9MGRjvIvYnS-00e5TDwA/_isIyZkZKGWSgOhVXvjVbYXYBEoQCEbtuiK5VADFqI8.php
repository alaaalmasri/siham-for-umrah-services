<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/shiam/footer.html.twig */
class __TwigTemplate_540fb92b40816dbf186524170667fe2f2b365697668e8501ce97784ac1592c6f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<footer>
    <div class=\"footer\">
        <div class=\"row\">
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"shema-address\">

                </div>
                <h3>How to Find US</h3>
                <div class=\"sheam-adress-content\">
                    <img src=\"";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "themes/shiam/assets/images/logo1.png\" class=\"img-responsive\">
                    <h2>ADDRESS</h2>
                    <div class=\"adress\">
                        ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "address", [])), "html", null, true);
        echo "
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"phone-number\">
                    <img src=\"";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "themes/shiam/assets/images/icon2.png\" class=\"img-responsive\">
                    <h3>Call US</h3>
                    <div class=\"phone-numbers\">
                      ";
        // line 22
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "phone", [])), "html", null, true);
        echo "
                    </div>
                </div>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"social-title\">
                    <h2><b>FOLLOW US</b></h2>
                </div>

                <div id=\"social-media\">
                 ";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "social_media", [])), "html", null, true);
        echo "
                </div>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"news-letter-title\">
                    <h2><b>Newsletter</b></h2>
                </div>
                <div class=\"news-letter-form\">
                    <div class=\"form\">
                        <input type=\"text\" class=\"form-control\" id=\"search\" placeholder=\"name\">
                        <div class=\"submit\">
                            <a href=\"#\">submit</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <div class=\"copyrights\">
        <p>Copyright @Fikra. All Right Reserved.</p>
    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "themes/shiam/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 32,  87 => 22,  81 => 19,  72 => 13,  66 => 10,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/shiam/footer.html.twig", "C:\\wamp64\\www\\siham\\themes\\shiam\\footer.html.twig");
    }
}
