<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__7bbc27e793d86a7bbb8c12d368d7e030f40fc8a6f8ccee9b735fcb1854845f74 */
class __TwigTemplate_ef85e5f067a78a2def467ca5e346e9958571e871602bbadcf57df8f8b8bcf981 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo " <div class=\"package\">
            <div class=\"container\">
     <div class=\"row\">
             <div class=\"node-package\">
                <p>";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_uniqe_title"] ?? null)), "html", null, true);
        echo " </p> 
             </div>
             <div class=\"node-dur\">
                 <p>";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_dur"] ?? null)), "html", null, true);
        echo "</p>
             </div>
</div>
     </div>
 </div>
<div class=\"package-info\">
<div class=\"package-imgs\">
";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_image"] ?? null)), "html", null, true);
        echo "
</div>
<div class=\"package-day\">
";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_day"] ?? null)), "html", null, true);
        echo " 
</div>
</div>
<div class=\"pages-title\">
<div class=\"title\">
<p>Detailed Price</p>
</div>
</div>
<div class=\"page-table\">
";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_table"] ?? null)), "html", null, true);
        echo "
</div>
<div class=\"pages-title\">
<div class=\"title\">
<p>Seasonality </p>
</div>
</div>
<div class=\"page-table\">
";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_seasonality__table"] ?? null)), "html", null, true);
        echo " 
</div>
<div class=\"pages-title\">
<div class=\"title\">
<p>Prices </p>
</div>
</div>
<div class=\"page-table\">
";
        // line 43
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_price_table"] ?? null)), "html", null, true);
        echo "
</div>
<div class=\"pages-title\">
<div class=\"title\">
<p>Package Inclusions and Exclusions</p>
</div>
</div>
<div class=\"package-info-page\">
<p>More than 100 employees working for Siham , provide the best possible services to the people come from all over the world to perform Umrah in Saudi Arabia. Services that include  but not limited,</p>
</div>
";
    }

    public function getTemplateName()
    {
        return "__string_template__7bbc27e793d86a7bbb8c12d368d7e030f40fc8a6f8ccee9b735fcb1854845f74";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 43,  106 => 35,  95 => 27,  83 => 18,  77 => 15,  67 => 8,  61 => 5,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__7bbc27e793d86a7bbb8c12d368d7e030f40fc8a6f8ccee9b735fcb1854845f74", "");
    }
}
